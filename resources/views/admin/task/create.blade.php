<x-admin>
    @section('title', 'Create Task')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Create Task</h3>
            <div class="card-tools"><a href="{{ route('admin.task.index') }}" class="btn btn-sm btn-dark">Back</a></div>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.task.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="taskName" class="form-label">Name:*</label>
                            <input type="text" class="form-control" name="taskName" required
                                value="{{ old('taskName') }}">
                            @error('name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="taskDetails" class="form-label">Task Details:*</label>
                            <textarea class="form-control" name="taskDetails" required
                                      value="{{ old('taskDetails') }}"></textarea>
                            @error('email')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="float-right">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-admin>
