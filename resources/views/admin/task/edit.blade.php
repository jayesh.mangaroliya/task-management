<x-admin>
    @section('title', 'Edit Task')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Edit Task</h3>
            <div class="card-tools"><a href="{{ route('admin.task.index') }}" class="btn btn-sm btn-dark">Back</a></div>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.task.update',$task) }}" method="POST">
                @method('PUT')
                @csrf
                <input type="hidden" name="id" value="{{ $task->id }}">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="taskName" class="form-label">Name:*</label>
                            <input type="text" class="form-control" name="taskName" required
                                   value="{{ $task->taskName }}">
                            @error('name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="taskDetails" class="form-label">Email:*</label>
                            <textarea class="form-control" name="taskDetails" required
                                      >{{ $task->taskDetails }}</textarea>
                            @error('email')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="float-right">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-admin>
