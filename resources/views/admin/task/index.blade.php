<x-admin>
    @section('title', 'Task')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Task Table</h3>
            <div class="card-tools"><a href="{{ route('admin.task.create') }}" class="btn btn-sm btn-primary">Add</a></div>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="taskTable">
                <thead>
                    <tr>
                        <th>#</th>
                        @role('admin')
                        <th>User Name</th>
                        @endrole
                        <th>Task Name</th>
                        <th>Task Details</th>
                        <th>Created</th>
                        <th>Action</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $task)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            @role('admin')
                            @foreach($task->user as $uName)
                                <td>{{ $uName->name }}</td>
                            @endforeach
                            @endrole
                            <td>{{ $task->taskName }}</td>
                            <td>{{ $task->taskDetails }}</td>
                            <td>{{ $task->created_at }}</td>
                            <td>
                                <a href="{{ route('admin.task.edit', encrypt($task->id)) }}"
                                    class="btn btn-sm btn-primary">Edit</a>
                            </td>
                            <td>
                                <form action="{{ route('admin.task.destroy', encrypt($task->id)) }}" method="POST"
                                    onsubmit="return confirm('Are sure want to delete?')">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @section('js')
        <script>
            $(function() {
                $('#taskTable').DataTable({
                    "paging": true,
                    "searching": true,
                    "ordering": true,
                    "responsive": true,
                });
            });
        </script>
    @endsection
</x-admin>
