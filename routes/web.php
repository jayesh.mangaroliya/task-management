<?php

use App\Http\Controllers\LoginWithOTPController;
use App\Http\Controllers\SocialiteController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $readmePath = base_path('README.md');

    return view('welcome', [
        'readmeContent' => Str::markdown(file_get_contents($readmePath)),
    ]);
});



// Auth routes
require __DIR__.'/auth.php';
// Admin Routes
require('admin.php');
