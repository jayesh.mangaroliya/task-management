<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->user()->hasAnyRole(['admin'])){
            $data = Task::with('user')->orderBy('id','DESC')->get();
            return view('admin.task.index', compact('data'));
        } else {
            $data = Task::where('userId',Auth::id())->orderBy('id','DESC')->get();
            return view('admin.task.index', compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.task.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'taskName' => 'required',
            'taskDetails' => 'required',
        ]);

        Task::create([
            'userId' => Auth::id(),
            'taskName' => $request->taskName,
            'taskDetails' => $request->taskDetails,
        ]);
        return redirect()->route('admin.task.index')->with('success','Task created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $task = Task::where('id',decrypt($id))->first();
        return view('admin.task.edit',compact('task'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Task $task)
    {
        $request->validate([
            'taskName' => 'required',
            'taskDetails' => 'required',
        ]);
        $user = Task::find($request->id);
        $user->taskName = $request->taskName;
        $user->taskDetails = $request->taskDetails;
        $user->save();
        return redirect()->route('admin.task.index')->with('success','Task updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Task::where('id',decrypt($id))->delete();
        return redirect()->back()->with('success','Task deleted successfully.');
    }
}
